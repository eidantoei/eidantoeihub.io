#!/bin/bash

pip install --upgrade awscli
mkdir ~/.aws
echo -e "[preview]\ncloudfront = true" > ~/.aws/config

aws s3 sync --delete ${CI_PROJECT_DIR} s3://eidantoei.org-static-host/ --exclude ".git/*"
aws cloudfront create-invalidation --distribution-id ${AWS_CF_DIST_ID} --invalidation-batch "{\"Paths\": {\"Quantity\": 2,\"Items\": [\"/\",\"/index.html\"]},\"CallerReference\": \"gitlab-ci-$(date ''+%Y%m%d-%H%M)\"}"
